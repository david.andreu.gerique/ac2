--
-- Copyright (c) 2018, UPC
-- All rights reserved.
-- 

library ieee;		
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; 
use ieee.math_real.all;	
use work.all;		
		
entity pruebaS4bits is 		

end pruebaS4bits;		
		
architecture prueba of pruebaS4bits is		
		
component S4bits is 		
generic(ret_xor: time := 15 ns; ret_and: time := 10 ns; ret_or: time := 10 ns);
port (A: in std_logic_vector(3 downto 0);
	B: in std_logic_vector(3 downto 0);
	cen: in	std_logic;
	SUM: out std_logic_vector(3 downto 0);
	csal: out std_logic);
end component;			

signal a, b, suma: std_logic_vector (3 downto 0);
signal centrada, csortida: std_logic;

signal csal_SUM: std_logic_vector(4 downto 0);
signal reloj: std_logic;
signal final: std_logic := '0';

begin	
	sumador_prova: S4bits port map(A=>a, B=>b, cen=>centrada, csal=>csortida, SUM=>suma);
	
	csal_SUM <= csortida & suma;
	
prueba: process
variable bits: std_logic_vector (8 downto 0) := "000000000";
variable gray_bits: std_logic_vector (8 downto 0) := "000000000";
variable bits_max: std_logic_vector (8 downto 0) := "000000000";
variable t_retardo: time;
variable retardo_max: time;
variable periode: time := 400 ns;

begin	
	for x in 0 to 511 loop
	
		bits := std_logic_vector(to_unsigned(x,9));
		
		for k in 0 to 7 loop
			gray_bits(k) := bits(k) xor bits(k + 1);
		end loop;
		gray_bits(8) := bits(8);
		
		a <= gray_bits(3 downto 0);
		b <= gray_bits(7 downto 4);
		centrada <= gray_bits(8);
		
		wait until reloj = '0' and reloj'event;
		wait until reloj = '1' and reloj'event;
		
		if csal_SUM'last_event = suma(2)'last_event then
			t_retardo := periode - csal_SUM'last_event;
			if bits = "000000000" then
				retardo_max := t_retardo;
			elsif t_retardo > retardo_max then
				retardo_max := t_retardo;
				bits_max := gray_bits;
			end if;
		end if;
	
	end loop;
	
	report "retard maxim: " & time'image(retardo_max) 
		& " amb valor A: " & integer'image(to_integer(unsigned(bits_max(3 downto 0)))) 
		& " amb valor B: " & integer'image(to_integer(unsigned(bits_max(7 downto 4))))
		& " amb valor cen: " & std_logic'image(bits_max(8))
	;
	
	final <= '1';
	wait;
end process;

rlj: process 	
variable iter : integer := 0;	
begin	
	reloj <= '1'; wait for periododiv2;	
	reloj <= '0'; wait for periododiv2;	
	if final = '1' then	
			wait;	
	end if;	
end process;

end prueba;		

