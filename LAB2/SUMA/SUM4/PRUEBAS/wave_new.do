onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Entrades
add wave -noupdate /pruebaS4bits/a
add wave -noupdate /pruebaS4bits/b
add wave -noupdate -format Literal /pruebaS4bits/centrada
add wave -noupdate -divider Sortides
add wave -noupdate /pruebaS4bits/csal_SUM
--add wave -noupdate /pruebaS4bits/suma
--add wave -noupdate -format Literal /pruebaS4bits/csortida
add wave -noupdate -divider Internes
add wave -noupdate -format Literal /pruebaS4bits/sumador_prova/c1
add wave -noupdate -format Literal /pruebaS4bits/sumador_prova/c2
add wave -noupdate -format Literal /pruebaS4bits/sumador_prova/c3
add wave -noupdate -format Literal /pruebaS4bits/sumador_prova/c4
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 184
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 40000
configure wave -gridperiod 80000
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1296750 ps}
