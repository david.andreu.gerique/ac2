library ieee;
use ieee.std_logic_1164.all;

entity s1bit is
  port (
    x, y: in  std_logic;
    cen:  in  std_logic;
    s:    out std_logic;
    csal: out std_logic);
end s1bit;

architecture entrega of s1bit is
  constant retard_xor: time := 15ns;
  constant retard_and: time := 10ns;
  constant retard_or:  time := 15ns;
  signal xor_xy, and_xy, and_xorxy_cen: std_logic;
begin
  xor_xy        <= x xor y                 after retard_xor;
  s             <= xor_xy xor cen          after retard_xor;
  and_xy        <= x and y                 after retard_and;
  and_xorxy_cen <= xor_xy and cen          after retard_and;
  csal          <= and_xorxy_cen or and_xy after retard_or;
end entrega;