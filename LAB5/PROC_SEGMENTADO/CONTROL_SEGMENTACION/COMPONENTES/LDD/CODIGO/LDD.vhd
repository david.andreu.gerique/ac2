--
-- Copyright (c) 2018, UPC
-- All rights reserved.
-- 

library ieee;
use ieee.std_logic_1164.all;

use work.param_disenyo_pkg.all;
use work.cte_tipos_deco_camino_pkg.all;
use work.componentes_control_seg_pkg.all;
use work.retardos_cntl_seg_pkg.all;

entity LDD is
	port(IDL1, IDL2 : in dir_reg; 
		valIDL1, valIDL2 : in std_logic;
		rdA, rdM, rdF, rdE : in dir_reg; 
		PBRA, PBRM, PBRF, PBRE : in st_PBR;
		IDL1A, IDL1M, IDL1F, IDL1E: out std_logic;
		IDL2A, IDL2M, IDL2F, IDL2E: out std_logic);
end LDD;


architecture estructural of LDD is

signal cum_10: std_logic;
signal cum_20: std_logic;

signal cum_1A: std_logic;
signal cum_1M: std_logic;
signal cum_1F: std_logic;
signal cum_1E: std_logic;
signal cum_2A: std_logic;
signal cum_2M: std_logic;
signal cum_2F: std_logic;
signal cum_2E: std_logic;

begin
	CUMP_10: cmp port map (a => IDL1, b => "00000", ig => cum_10);
	CUMP_20: cmp port map (a => IDL2, b => "00000", ig => cum_20);

	CUMP_1A: cmp port map (a => IDL1, b => rdA, ig => cum_1A);
	CUMP_1M: cmp port map (a => IDL1, b => rdM, ig => cum_1M);
	CUMP_1F: cmp port map (a => IDL1, b => rdF, ig => cum_1F);
	CUMP_1E: cmp port map (a => IDL1, b => rdE, ig => cum_1E);
	CUMP_2A: cmp port map (a => IDL2, b => rdA, ig => cum_2A);
	CUMP_2M: cmp port map (a => IDL2, b => rdM, ig => cum_2M);
	CUMP_2F: cmp port map (a => IDL2, b => rdF, ig => cum_2F);
	CUMP_2E: cmp port map (a => IDL2, b => rdE, ig => cum_2E);

	IDL1A <= '1' when PBRA = PBR_si and valIDL1 = '1' and cum_1A = '1' and cum_10 = '0' else '0' after retLDD;
	IDL1M <= '1' when PBRM = PBR_si and valIDL1 = '1' and cum_1M = '1' and cum_10 = '0' else '0' after retLDD;
	IDL1F <= '1' when PBRF = PBR_si and valIDL1 = '1' and cum_1F = '1' and cum_10 = '0' else '0' after retLDD;
	IDL1E <= '1' when PBRE = PBR_si and valIDL1 = '1' and cum_1E = '1' and cum_10 = '0' else '0' after retLDD;
	
	IDL2A <= '1' when PBRA = PBR_si and valIDL2 = '1' and cum_2A = '1' and cum_20 = '0' else '0' after retLDD;
	IDL2M <= '1' when PBRM = PBR_si and valIDL2 = '1' and cum_2M = '1' and cum_20 = '0' else '0' after retLDD;
	IDL2F <= '1' when PBRF = PBR_si and valIDL2 = '1' and cum_2F = '1' and cum_20 = '0' else '0' after retLDD;
	IDL2E <= '1' when PBRE = PBR_si and valIDL2 = '1' and cum_2E = '1' and cum_20 = '0' else '0' after retLDD;
	
end estructural;
