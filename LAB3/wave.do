onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider reloj
add wave -noupdate /prueba_camino_control/reloj
add wave -noupdate -divider control
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/controlDE/IDL1
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/controlDE/IDL2
add wave -noupdate /prueba_camino_control/camino_cnt/controlDE/PE
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/controlDE/IDE
add wave -noupdate -divider {camino de datos}
add wave -noupdate -radix unsigned -childformat {{/prueba_camino_control/camino_cnt/bancoregistro/IDL1(4) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/IDL1(3) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/IDL1(2) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/IDL1(1) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/IDL1(0) -radix unsigned}} -subitemconfig {/prueba_camino_control/camino_cnt/bancoregistro/IDL1(4) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/IDL1(3) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/IDL1(2) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/IDL1(1) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/IDL1(0) {-radix unsigned}} /prueba_camino_control/camino_cnt/bancoregistro/IDL1
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/bancoregistro/IDL2
add wave -noupdate -radix unsigned -childformat {{/prueba_camino_control/camino_cnt/bancoregistro/LE1(7) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/LE1(6) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/LE1(5) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/LE1(4) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/LE1(3) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/LE1(2) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/LE1(1) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/LE1(0) -radix unsigned}} -subitemconfig {/prueba_camino_control/camino_cnt/bancoregistro/LE1(7) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/LE1(6) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/LE1(5) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/LE1(4) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/LE1(3) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/LE1(2) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/LE1(1) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/LE1(0) {-radix unsigned}} /prueba_camino_control/camino_cnt/bancoregistro/LE1
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/bancoregistro/LE2
add wave -noupdate -radix unsigned -childformat {{/prueba_camino_control/camino_cnt/DEsum(7) -radix unsigned} {/prueba_camino_control/camino_cnt/DEsum(6) -radix unsigned} {/prueba_camino_control/camino_cnt/DEsum(5) -radix unsigned} {/prueba_camino_control/camino_cnt/DEsum(4) -radix unsigned} {/prueba_camino_control/camino_cnt/DEsum(3) -radix unsigned} {/prueba_camino_control/camino_cnt/DEsum(2) -radix unsigned} {/prueba_camino_control/camino_cnt/DEsum(1) -radix unsigned} {/prueba_camino_control/camino_cnt/DEsum(0) -radix unsigned}} -subitemconfig {/prueba_camino_control/camino_cnt/DEsum(7) {-radix unsigned} /prueba_camino_control/camino_cnt/DEsum(6) {-radix unsigned} /prueba_camino_control/camino_cnt/DEsum(5) {-radix unsigned} /prueba_camino_control/camino_cnt/DEsum(4) {-radix unsigned} /prueba_camino_control/camino_cnt/DEsum(3) {-radix unsigned} /prueba_camino_control/camino_cnt/DEsum(2) {-radix unsigned} /prueba_camino_control/camino_cnt/DEsum(1) {-radix unsigned} /prueba_camino_control/camino_cnt/DEsum(0) {-radix unsigned}} /prueba_camino_control/camino_cnt/DEsum
add wave -noupdate /prueba_camino_control/camino_cnt/bancoregistro/PE
add wave -noupdate -radix unsigned -childformat {{/prueba_camino_control/camino_cnt/bancoregistro/IDE(4) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/IDE(3) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/IDE(2) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/IDE(1) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/IDE(0) -radix unsigned}} -subitemconfig {/prueba_camino_control/camino_cnt/bancoregistro/IDE(4) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/IDE(3) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/IDE(2) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/IDE(1) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/IDE(0) {-radix unsigned}} /prueba_camino_control/camino_cnt/bancoregistro/IDE
add wave -noupdate -radix unsigned -childformat {{/prueba_camino_control/camino_cnt/bancoregistro/DE(7) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/DE(6) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/DE(5) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/DE(4) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/DE(3) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/DE(2) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/DE(1) -radix unsigned} {/prueba_camino_control/camino_cnt/bancoregistro/DE(0) -radix unsigned}} -subitemconfig {/prueba_camino_control/camino_cnt/bancoregistro/DE(7) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/DE(6) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/DE(5) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/DE(4) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/DE(3) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/DE(2) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/DE(1) {-radix unsigned} /prueba_camino_control/camino_cnt/bancoregistro/DE(0) {-radix unsigned}} /prueba_camino_control/camino_cnt/bancoregistro/DE
add wave -noupdate -divider registros
add wave -noupdate /prueba_camino_control/camino_cnt/bancoregistro/idedeco
add wave -noupdate /prueba_camino_control/camino_cnt/bancoregistro/mem(19)
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Flanc de rellotge} {1825000 ps} 1} {{Sortides del bloc de control} {1827000 ps} 1} {{Dades de lectura del banc de registre} {1837000 ps} 1} {Suma {1853000 ps} 1} {{Decoder per identificar el registre d'escriptura} {1835000 ps} 1} {{Flanc de rellotge següent} {1875000 ps} 1} {{Escriptura al banc de registres} {1889000 ps} 1}
quietly wave cursor active 0
configure wave -namecolwidth 443
configure wave -valuecolwidth 50
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 25000
configure wave -gridperiod 50000
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1816987 ps} {1898459 ps}
