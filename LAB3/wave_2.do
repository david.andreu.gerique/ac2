onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider reloj
add wave -noupdate /prueba_camino_control/cntciclos
add wave -noupdate -divider {senyales externas}
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/IDL1ini
add wave -noupdate /prueba_camino_control/camino_cnt/PEini
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/IDEini
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/DEini
add wave -noupdate -divider control
add wave -noupdate /prueba_camino_control/camino_cnt/controlDE/Pcero
add wave -noupdate /prueba_camino_control/camino_cnt/controlDE/ini
add wave -noupdate /prueba_camino_control/camino_cnt/controlDE/finalop
add wave -noupdate -format Literal /prueba_camino_control/camino_cnt/controlDE/estado
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/controlDE/cnt
add wave -noupdate /prueba_camino_control/reloj
add wave -noupdate -divider {camino de datos}
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/bancoregistro/IDL1
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/bancoregistro/IDL2
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/bancoregistro/LE1
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/bancoregistro/LE2
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/DEsum
add wave -noupdate /prueba_camino_control/camino_cnt/bancoregistro/PE
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/bancoregistro/IDE
add wave -noupdate -radix unsigned /prueba_camino_control/camino_cnt/bancoregistro/DE
add wave -noupdate -divider registros
add wave -noupdate /prueba_camino_control/camino_cnt/bancoregistro/mem(10)
add wave -noupdate /prueba_camino_control/camino_cnt/bancoregistro/idedeco
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Flanc de rellotge} {1875000 ps} 1} {{Sortides bloc control} {1877000 ps} 1} {{Sortida dada LE1} {1887000 ps} 1} {{Sortida dada LE2} {1899000 ps} 1} {Suma {1915000 ps} 1} {{Flanc de rellotge següent} {1925000 ps} 1} {{Escriptura en el banc de reg} {1939000 ps} 1}
quietly wave cursor active 0
configure wave -namecolwidth 242
configure wave -valuecolwidth 49
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 25000
configure wave -gridperiod 50000
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1871217 ps} {1945079 ps}
